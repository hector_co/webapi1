﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Tracing;
using WebApi1.DomainLayerObjects;
using WebApi1.Models;

namespace WebApi1.Controllers
{
    [RoutePrefix("api/customers")]
    public class CustomersController : ApiController
    {
        private static List<Customer> Customers
        {
            get
            {
                if (HttpContext.Current.Application["customers"] == null)
                {
                    HttpContext.Current.Application["customers"] = new List<Customer>
                    {
                        new Customer
                        {
                            Id = 1,
                            Name = "Customer1",
                            LastName = "Lastname Customer1",
                            PhoneNumbers=new List<PhoneNumber>
                            {
                                new PhoneNumber
                                {
                                    Description="home",
                                    Number = "1111"
                                },
                                new PhoneNumber
                                {
                                    Description="Movile",
                                    Number = "71111"
                                }
                            }
                        },
                        new Customer
                        {
                            Id = 2,
                            Name = "Customer2",
                            LastName = "Lastname Customer2",
                            PhoneNumbers=new List<PhoneNumber>
                            {
                                new PhoneNumber
                                {
                                    Description="home",
                                    Number = "2222"
                                },
                                new PhoneNumber
                                {
                                    Description="Movile",
                                    Number = "72222"
                                }
                            }
                        },
                        new Customer
                        {
                            Id = 3,
                            Name = "Customer3",
                            LastName = "Lastname Customer3",
                            PhoneNumbers=new List<PhoneNumber>
                            {
                                new PhoneNumber
                                {
                                    Description="home",
                                    Number = "3333"
                                },
                                new PhoneNumber
                                {
                                    Description="Movile",
                                    Number = "73333"
                                }
                            }
                        }
                    };
                }
                return HttpContext.Current.Application["customers"] as List<Customer>;
            }
        }

        private static int CustomerIdCounter
        {
            get
            {
                if (HttpContext.Current.Application["customerIdCounter"] == null)
                {
                    HttpContext.Current.Application["customerIdCounter"] = 4;
                }
                return int.Parse(HttpContext.Current.Application["customerIdCounter"].ToString());
            }
            set { HttpContext.Current.Application["customerIdCounter"] = value; }
        }

        [HttpGet]
        [Route("LogSample")]
        public IHttpActionResult LogSample()
        {
            Configuration.Services.GetTraceWriter().Warn(Request, "CustomersControllerWarning", "action LogSample");
            return Ok();
        }

        [HttpGet]
        [Route("ErrorSample")]
        public IHttpActionResult ErrorSample()
        {
            throw new Exception("Error generado intencionalmente...");
        }

        #region Read
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(Customers.OrderBy(e => e.Id).Select(e => new { e.Id, e.Name, e.LastName }).ToList());
        }

        [HttpGet]
        [Route("{id}", Name = "GetCustomer")]
        public IHttpActionResult Get(int id)
        {
            var customer = Customers.FirstOrDefault(e => e.Id == id);
            if (customer == null)
            {
                return NotFound();
            }
            return Ok(new { customer.Id, customer.Name, customer.LastName });
        }

        [HttpGet]
        [Route("{id}/PhoneNumbers")]
        public IHttpActionResult GetPhoneNumbers(int id)
        {
            var customer = Customers.FirstOrDefault(e => e.Id == id);
            if (customer == null)
            {
                return NotFound();
            }
            return Ok(customer.PhoneNumbers.Select(p => new { p.Description, p.Number }).ToList());
        }

        [HttpGet]
        [Route("{id}/PhoneNumbers/{description:nonspaces}", Name = "GetPhoneNumber")]
        public IHttpActionResult GetPhoneNumber(int id, string description)
        {
            var customer = Customers.FirstOrDefault(e => e.Id == id);
            if (customer == null)
            {
                return NotFound();
            }
            var phoneNumber =
                customer.PhoneNumbers.FirstOrDefault(
                    p => string.Equals(p.Description, description, StringComparison.CurrentCultureIgnoreCase));
            if (phoneNumber == null)
            {
                return NotFound();
            }
            return Ok(new { phoneNumber.Description, phoneNumber.Number });
        }
        #endregion

        #region Write
        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromBody] CustomerModel customer)
        {
            customer.Id = CustomerIdCounter++;
            Customers.Add(new Customer { Id = customer.Id, Name = customer.Name, LastName = customer.LastName });

            var link = Url.Link("GetCustomer", new Dictionary<string, object> { { "id", customer.Id } });
            return Created(link, customer);
        }

        [HttpPut]
        [Route("{id}")]
        public IHttpActionResult Put(int id, [FromBody]CustomerModel customer)
        {
            var existentCustomer = Customers.FirstOrDefault(e => e.Id == id);
            if (existentCustomer == null)
            {
                return NotFound();
            }
            Customers.Remove(existentCustomer);
            customer.Id = id;
            Customers.Add(new Customer { Id = customer.Id, Name = customer.Name, LastName = customer.LastName });
            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult Delete(int id)
        {
            var existentCustomer = Customers.FirstOrDefault(e => e.Id == id);
            if (existentCustomer == null)
            {
                return NotFound();
            }
            Customers.Remove(existentCustomer);
            return Ok();
        }

        [HttpPost]
        [Route("{id}/PhoneNumbers")]
        public IHttpActionResult AddPhoneNumber(int id, [FromBody] PhoneNumber phoneNumber)
        {
            var customer = Customers.FirstOrDefault(e => e.Id == id);
            if (customer == null)
            {
                return NotFound();
            }
            var existentPhoneNumber =
                customer.PhoneNumbers.FirstOrDefault(
                    p =>
                        string.Equals(p.Description, phoneNumber.Description, StringComparison.CurrentCultureIgnoreCase));
            if (existentPhoneNumber != null)
            {
                return Conflict();
            }

            customer.PhoneNumbers.Add(phoneNumber);

            var link = Url.Link("GetPhoneNumber",
                new Dictionary<string, object> { { "id", customer.Id }, { "description", phoneNumber.Description } });
            return Created(link, phoneNumber);
        }

        [HttpPut]
        [Route("{id}/PhoneNumbers")]
        public IHttpActionResult UpdatePhoneNumber(int id, [FromBody] PhoneNumber phoneNumber)
        {
            var customer = Customers.FirstOrDefault(e => e.Id == id);
            if (customer == null)
            {
                return NotFound();
            }
            var existentPhoneNumber =
                customer.PhoneNumbers.FirstOrDefault(
                    p =>
                        string.Equals(p.Description, phoneNumber.Description, StringComparison.CurrentCultureIgnoreCase));
            if (existentPhoneNumber == null)
            {
                return NotFound();
            }

            existentPhoneNumber.Number = phoneNumber.Number;
            return Ok();
        }

        [HttpDelete]
        [Route("{id}/PhoneNumbers/{description:nonspaces}")]
        public IHttpActionResult DeletePhoneNumber(int id, string description)
        {
            var customer = Customers.FirstOrDefault(e => e.Id == id);
            if (customer == null)
            {
                return NotFound();
            }
            var phoneNumber =
                customer.PhoneNumbers.FirstOrDefault(
                    p => string.Equals(p.Description, description, StringComparison.CurrentCultureIgnoreCase));
            if (phoneNumber == null)
            {
                return NotFound();
            }
            customer.PhoneNumbers.Remove(phoneNumber);
            return Ok();
        }
        #endregion

    }

}
