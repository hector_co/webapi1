﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace WebApi1.Infrastructure
{
    public class SanitizeExceptionHandler : IExceptionHandler
    {
        public Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            Handle(context);
            return Task.FromResult(0);
        }

        private static void Handle(ExceptionHandlerContext context)
        {
            context.Result = new ResponseMessageResult(
                new HttpResponseMessage
                {
                    Content = new StringContent(context.Exception.Message)
                });
        }
    }
}
