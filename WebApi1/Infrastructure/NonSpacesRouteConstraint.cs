﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace WebApi1.Infrastructure
{
    public class NonSpacesRouteConstraint : IHttpRouteConstraint
    {
        public bool Match(HttpRequestMessage request, IHttpRoute route, string parameterName, IDictionary<string, object> values,
            HttpRouteDirection routeDirection)
        {
            var value = values[parameterName].ToString();

            if (value.Contains(" "))
            {
                throw new HttpResponseException(HttpStatusCode.PreconditionFailed);
            }

            return true;
        }
    }
}
