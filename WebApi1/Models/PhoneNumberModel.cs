﻿
namespace WebApi1.Models
{
    public class PhoneNumberModel
    {
        public string Description { get; set; }
        public string Number { get; set; }
    }
}
