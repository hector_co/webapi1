﻿
namespace WebApi1.DomainLayerObjects
{
    public class PhoneNumber
    {
        public string Description { get; set; }
        public string Number { get; set; }
    }
}
