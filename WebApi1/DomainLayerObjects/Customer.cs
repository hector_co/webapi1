﻿using System.Collections.Generic;

namespace WebApi1.DomainLayerObjects
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public List<PhoneNumber> PhoneNumbers { get; set; }

        public Customer()
        {
            PhoneNumbers = new List<PhoneNumber>();
        }
    }
}
